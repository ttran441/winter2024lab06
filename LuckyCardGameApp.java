import java.util.Scanner;

public class LuckyCardGameApp {
	
	public static void main(String[] args) {
		
		GameManager gm = new GameManager();
		int totalPoints = 0;
		int roundCount = 1;
		String starBorder = "************************************";
		
		System.out.println("Welcome to the lucky card game!");
		
		while(gm.getDrawPile().length() > 1 && totalPoints < 5) {
			
			System.out.println("Round: " + roundCount);
			
			System.out.println(gm);
			totalPoints += gm.calculatePoints();
			System.out.println("Your points after round " + roundCount + ": " + totalPoints);
			System.out.println(starBorder);
			gm.dealCards();
			roundCount++;
			
		}
		
		if(totalPoints < 5) {
			System.out.println("You lost with " + totalPoints + " points!");
		}
		else {
			System.out.println("You won with " + totalPoints + " points!");
		}
		
	}
	
}