import java.util.Random;

public class Deck {
	
	private Card[] cards; 
	private int numberOfCards;
	private Random rng;
	
	public Deck() {
		
		numberOfCards = 52;
		cards = new Card[numberOfCards];
		rng = new Random();
		
		String[] suits = {"Hearts", "Diamonds", "Spades", "Clubs"};
		int cardsPerSuit = 13;
		String[] value = {"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		
		for(int i = 0; i < numberOfCards; i++) {
			
			int suitIndex = i / cardsPerSuit;
			
			int valueIndex = i % 13;
			
			this.cards[i] = new Card(suits[suitIndex], value[valueIndex]);
			
		}
		
	}
	
	public int length() {
		
		return this.numberOfCards;
		
	}
	
	public Card drawTopCard() {
		
		Card lastCard = this.cards[this.numberOfCards-1];
		this.numberOfCards--;
		return lastCard;
		
	}
	
	public String toString() {
		
		String cardString = "";
		
		for(int i = 0; i < this.numberOfCards; i++) {
			cardString += this.cards[i] + "\n";
		}
		
		return cardString;
		
	}
	
	public void shuffle() {
		
		for(int i = 0; i < this.numberOfCards; i++) {
			
			int randPos = rng.nextInt(this.numberOfCards - i) + i;
			Card cardToSwap = this.cards[i];
			this.cards[i] = this.cards[randPos];
			this.cards[randPos] = cardToSwap;
			
		}
		
	}
	
}