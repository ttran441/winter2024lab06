public class GameManager {
	
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager() {
		
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
		
	}
	
	public Deck getDrawPile() {
		
		return this.drawPile;
		
	}
	
	public Card getPlayerCard() {
		
		return this.playerCard;
		
	}
	
	public Card getCenterCard() {
		
		return this.centerCard;
		
	}
	
	public String toString() {
		
		String border = "------------------------------------";
		return border + "\nCenter card: " + this.centerCard + "\nPlayer Card: " + this.playerCard + "\n" + border;
		
	}
	
	public void dealCards() {
		
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
		
	}
	
	public int getNumberOfCards() {
		
		return this.drawPile.length();
		
	}
	
	public int calculatePoints() {
		
		if(centerCard.getValue().equals(playerCard.getValue())) {
			
			return 4;
			
		}
		
		if(centerCard.getSuit().equals(playerCard.getSuit())) {
			
			return 2;
			
		}
		
		return -1;
		
	}
	
}